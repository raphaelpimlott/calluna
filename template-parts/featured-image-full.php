<?php
// If a featured image is set, insert into layout and use Interchange
// to select the optimal image size per named media query.
if ( has_post_thumbnail( $post->ID ) ) : ?>
<div class="full-height" style="background:url(
<?php
the_post_thumbnail_url();
?>
) no-repeat center center;
  -webkit-background-size: cover;
  -moz-background-size: cover;
  -o-background-size: cover;
  background-size: cover;">
	<h1 class="entry-title"><?php the_title(); ?></h1>
</div>
<?php endif;
