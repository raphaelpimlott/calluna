<?php
/*
Template Name: Full Width Home
*/
get_header(); ?>

<?php get_template_part( 'template-parts/featured-image-full' ); ?>
<!--
<div class="main-container">
-->
<div class="contentWrapper">
	<div class="grid-x">
		<div class="small-12 medium-4 cell">
			<p><?php the_field('main_text'); ?></p>
		</div>
		<div class="small-12 medium-4 cell">
		</div>
		<div class="small-12 medium-4 cell">
		<?php $imgFloat = get_field('first_flt_img') ?>
			<div class="parollerContainer">
				<img class="paroller" data-paroller-factor="0.15" data-paroller-type="foreground" data-paroller-direction="vertical" src="<?php echo $imgFloat ?>" />
			</div>
		</div>
	</div>
</div>

<?php if( have_rows('home_section_repeater') ): ?>

	<?php while( have_rows('home_section_repeater') ): the_row(); 

		// vars
		$title = get_sub_field('rpt_title');
		$image = get_sub_field('rpt_img');
		$body = get_sub_field('rpt_body_txt');
		$float = get_sub_field('rpt_float')

		?>
		<div class="repeater full-height" style="background:url(<?php echo $image ?>) no-repeat center center;-webkit-background-size: cover;-moz-background-size: cover;-o-background-size: cover;background-size: cover;">
		</div>
		<div class="contentWrapper">
			<div class="grid-x">
				<div class="small-12 medium-4 cell">
					<?php if( $title ): ?>
						<h2 class="repeater entry-title"><?php echo $title ?></h2>
					<?php endif; ?>
					<p><?php echo $body ?></p>
				</div>
				<div class="small-12 medium-4 cell">
				</div>
				<div class="small-12 medium-4 cell">
				<?php if( $float ): ?>
					<div class="parollerContainer">
						<img class="paroller" data-paroller-factor="0.15" data-paroller-type="foreground" data-paroller-direction="vertical" src="<?php echo $float ?>" />
					</div>
				<?php endif ?>
				</div>
			</div>
		</div>
	<?php endwhile; ?>

<?php endif; ?>


<?php get_footer();
