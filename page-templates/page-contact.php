<?php
/*
Template Name: Contact Page
*/
get_header(); ?>


<!--
<div class="main-container">
-->
<div class="contentWrapper main-grid">
	<div class="padder">
		<div class="grid-x">
			<div class="small-12 medium-7 large-9 cell">
			<?php
				if ( has_post_thumbnail() ) {
					the_post_thumbnail();
				}
			?>
			</div>
			<div class="small-12 medium-5 large-3 cell">
				<?php while ( have_posts() ) : the_post(); ?>
					<?php get_template_part( 'template-parts/content', 'page' ); ?>
					<?php comments_template(); ?>
				<?php endwhile; ?>
			</div>
		</div>
	</div>
</div>
<?php get_footer();
