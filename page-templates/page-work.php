<?php
/*
Template Name: Work Page
*/
get_header(); ?>


<!--
<div class="main-container">
-->
<div class="contentWrapper main-grid">
	<div class="padder">
		<div class="grid-x grid-padding-x medium-up-2 large-up-3">
			<?php if( have_rows('gallery') ): ?>
				<?php while( have_rows('gallery') ): the_row(); 
					// vars
					$image = get_sub_field('image');
					$size = 'gallerySize';
				?>
				<div class="cell galleryCell">
				<?php
					echo wp_get_attachment_image( $image, $size );
				?>
				</div>
				<?php endwhile; ?>
				<?php endif; ?>
		</div>
	</div>
</div>
<?php get_footer();
